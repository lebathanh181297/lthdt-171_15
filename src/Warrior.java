public class Warrior extends Fighter {

	public Warrior(int baseHp, int wp) {
		super(baseHp, wp);
	}
	public double getCombatScore() {
		// TODO Auto-generated method stub
		if(super.getWp() == 1) 
			return super.getBaseHp();
		else {
			if(Utility.isPrime(Battle.GROUND)) 
				return 2*super.getBaseHp();
			else 
				return super.getBaseHp()/10;
		}
		
	}
    
}
