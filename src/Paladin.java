
public class Paladin extends Knight {

	public Paladin(int baseHp, int wp) {
		super(baseHp, wp);	
	}
	public static int fb(int n) {
		 if(n==1 || n==0 || n==2 ||n==3) 
		    	return n;
		   return fb(n-2)+fb(n-1); 
	}
	public static int checkFb(int base ) {
		int i, d=0;
		for(i=0; i<=base; i++) {
			if(base==fb(i)) {
				d++;
				break;
			}
		}
		if(d==1)
				return i+1;
		else 
			return 0;
		
	}
	@Override
	public double getCombatScore(){
		int a = checkFb(super.getBaseHp());
		if (a>2)
			return 1000 + a;
		else 
			return 3*super.getBaseHp();
	}
}


